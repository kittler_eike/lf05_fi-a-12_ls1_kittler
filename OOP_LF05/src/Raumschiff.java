import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int AndroidenAnzahl;
	private int HuelleInProzent;
	private String Schiffsname;
	private int LebenserhaltungssystemInProzent;
	private Ladungen ladungen;
	private ArrayList<Ladungen> Ladung = new ArrayList<Ladungen>();
	
	public Raumschiff() {
		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	public int getAndroidenAnzahl() {
		return AndroidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		AndroidenAnzahl = androidenAnzahl;
	}

	public int getHuelleInProzent() {
		return HuelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		HuelleInProzent = huelleInProzent;
	}

	public String getSchiffsname() {
		return Schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		Schiffsname = schiffsname;
	}

	public int getLebenshaltungssystemInProzent() {
		return LebenserhaltungssystemInProzent;
	}

	public void setLebenserhaltungssystemInProzent(int lebenshaltungssystemInProzent) {
		LebenserhaltungssystemInProzent = lebenshaltungssystemInProzent;
	}
	public void addLadung(Ladungen neueLadung) {
		Ladung.add(neueLadung);
	}
	public void raumschiffZustandAusgeben() {
		System.out.println("\nSchiffsname: "+this.Schiffsname+"");
        System.out.println("Photonentorpedoanzahl: "+this.photonentorpedoAnzahl);
        System.out.println("Energieversorgung in Przozent: "+this.energieversorgungInProzent);
        System.out.println("Schilde in Prozent: "+this.schildeInProzent);
        System.out.println("Lebenserhaltungssystem in Prozent: "+this.LebenserhaltungssystemInProzent);
        System.out.println("Anzahl der Androiden: "+this.AndroidenAnzahl);
	}
	public void ladungAusgeben() {
		System.out.println("Ladung: "+Ladung);
	}
	public void abschießenPhotonentorpedos(Raumschiff r) {
		if(photonentorpedoAnzahl < 1) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else if(photonentorpedoAnzahl >= 1) {
			photonentorpedoAnzahl --;
			System.out.println("--Photonentorpedos abgeschossen--");
			System.out.println("Wir besitzen jetzt noch: "+photonentorpedoAnzahl+" Photonentorpedos.");
			treffer(r);
		}
	}
	public void abschießenPhaserkanone(Raumschiff r) {
		if(energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else if(energieversorgungInProzent >= 50) {
			energieversorgungInProzent -= 50;
			System.out.println("--Phaserkanone abgeschossen--");
			System.out.println("Wir besitzen jetzt noch: "+energieversorgungInProzent+" Prozent der Energieversorgung.");
			treffer(r);
		}
	}
	public void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname()+" wurde getroffen!");
	}
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
	}
}
