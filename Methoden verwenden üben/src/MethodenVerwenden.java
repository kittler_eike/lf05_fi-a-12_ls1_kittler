import java.util.Scanner;

public class MethodenVerwenden {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String k;
		
		// Benutzereingaben lesen
		
		k = liesArtikel("Was m�chten Sie bestellen?: ");
		

		System.out.println("Geben Sie die Anzahl ein: ");
		int anzahl = myScanner.nextInt();

		System.out.println("Geben Sie den Nettopreis ein: ");
		double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", k, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", k, anzahl, bruttogesamtpreis, mwst, "%");

	}
	public static String liesArtikel(String eingabetext) {		//Klasse definieren
		Scanner myScanner = new Scanner(System.in);				//Methoden einsetzen (Scanner)
		System.out.println("Was m�chten Sie bestellen?");		// Mothode definieren (artikel= Scanner)
		String artikel = myScanner.next();						// Artikel wieder ausgegeben.
		return artikel;											
		
	}
	
}