﻿import java.util.Scanner;

class Fahrkartenautomat 
{
	public static void main(String[] args) {
		boolean endlos = true;
		while(endlos = true) {
			
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double Gesamt = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		double rückgeld = rueckgeldAusgeben(Gesamt, zuZahlenderBetrag);
		}
	}
	
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n"
				+ "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
				+ "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
				+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"
				+ "Ihre Wahl: ");
		int wähleTicket = tastatur.nextInt();
		double preis = 0;
		boolean richtig = true;
		while(richtig == true) {
			if(wähleTicket == 1) {
			preis = 2.90;
			break;
			}else if(wähleTicket == 2) {
			preis = 8.60;
			break;
			}else if(wähleTicket == 3) {
			preis = 23.50;
			break;
			}else {
				System.out.println(">>falsche Eingabe<<");
				wähleTicket = 1;
				preis = 2.90;
				richtig = false;
			}
		
		System.out.println("Anzahl der Tickets: ");
		int ticketAnzahl = tastatur.nextInt();
		
		if(ticketAnzahl <= 10 || ticketAnzahl == 0) {
			System.out.println("Du hast die richtige Anzahl an Tickets getroffen.");
			
		}else {
			ticketAnzahl = 1;
			System.out.println("!!!Fehler!!! Du kannst von jedem Ticket nur 1 - 10 kaufen. Du musst jetzt 1 Ticket kaufen. ^^");
		}
		
		double Ticketpreis = preis * ticketAnzahl;
		
		return Ticketpreis;
		}
		return preis;
		
	} 
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		String euro = "Euro";
		double eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.printf("Noch zu zahlen: %.2f %s\n", +(zuZahlenderBetrag- eingezahlterGesamtbetrag), euro);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			
			warte(500);	
		}
		
		System.out.println("\n\n");
		}
	
	public static double rueckgeldAusgeben(double Gesamt, double zuZahlenderBetrag) {
		double rückgabebetrag;
		rückgabebetrag = Gesamt - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
			
			
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		return rückgabebetrag;
	}
	
	public static void warte(int millisekunde) {
		System.out.print("=");
		try {
			Thread.sleep(millisekunde);
		}
		
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
