/*
	import java.util.Scanner; // Import der Klasse Scanner
	public class Uebungsblatt_Konsoleneingabe 
	{
 
 public static void main(String[] args) // Hier startet das Programm
 {
 // Aufgabe 1:
 // Neues Scanner-Objekt myScanner wird erstellt 
 Scanner myScanner = new Scanner(System.in); 
 
 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
 
 // Die Variable zahl1 speichert die erste Eingabe
 int zahl1 = myScanner.nextInt(); 
 
 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
 
 // Die Variable zahl2 speichert die zweite Eingabe
 int zahl2 = myScanner.nextInt(); 
 
 // Die Addition der Variablen zahl1 und zahl2 
 // wird der Variable ergebnis zugewiesen.
 int ergebnis = zahl1 + zahl2;
 int ergebnisMinus = zahl1 - zahl2;
 float ergebnisMal = (float)zahl1 * zahl2;
 float ergebnisGeteilt = zahl1 / zahl2;
		 		 
 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis); 
 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisMinus);
 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMal);
 System.out.print("\n\n\nErgebnis der Division lautet: ");
 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisGeteilt);
 myScanner.close();
 
 } 
}*/

//Aufgabe 2: 
import java.util.Scanner;
public class Uebungsblatt_Konsoleneingabe
{

public static void main(String[] args)
{
	Scanner myScanner = new Scanner (System.in);
	System.out.println("Bitte geben Sie Ihren Name ein: ");
	String name = myScanner.next();
	
	System.out.println("Bitte geben Sie Ihr Alter ein:  ");
	int alter = myScanner.nextInt();
	
	System.out.println("Mein Name ist "+name+" und ich bin "+alter+" Jahre alt.");
	
}	
}





